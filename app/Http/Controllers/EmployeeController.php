<?php

namespace App\Http\Controllers;

use App\Models\Employee;
use App\Models\Position;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;

class EmployeeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if (request()->ajax()) {
            $employee = Employee::with("position")->get();
            return datatables($employee)
                ->addIndexColumn()
                ->addColumn('action', function ($row) {
                    $btn = '<a href="' . route('employee.edit', $row->id) . '" class="edit btn btn-primary btn-sm">Edit</a>';
                    $btn .= '&nbsp;&nbsp;';
                    $btn .= '<a href="' . route('employee.show', $row->id) . '" class="edit btn btn-success btn-sm">Detail</a>';
                    $btn .= '&nbsp;&nbsp;';
                    $btn .= '<button type="button" class="delete deleteData btn btn-danger btn-sm" data-id="' . $row->id . '">Delete</button>';
                    return $btn;
                })
                ->addColumn('status', function ($row) {
                    if ($row->status == 0) {
                        return "Aktif";
                    }
                    else {
                        return "Tidak Aktif";
                    }
                })
                ->rawColumns(['action', 'status'])
                ->toJson();
        }
        return view("employee");
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $position = Position::all();
        $employee = new Employee();
        return view("employee.create", compact(
            "position",
            "employee"
        ));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validate = $this->validate($request, [
            "nama" => "string",
            "nip" => "numeric|unique:employees,nip",
            "nomor_hp" => "numeric",
            "foto_ktp" => "required|mimes:png,jpg"
        ]);

        DB::beginTransaction();
        try {
            $data = $request->all();

            $foto = Storage::disk('public_uploads')->put('img/file_upload', $request->foto_ktp);
            $data['foto_ktp'] = $foto;
    
            $employee = Employee::create($data);
            DB::commit();
            return redirect()->route('employee.index')->with("success", "Berhasil Menambahkan Karyawan");
        } catch (\Throwable $th) {
            DB::rollBack();
            return redirect()->back()->with("danger", "Gagal! Terjadi kesalahan");
        }

    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Employee  $employee
     * @return \Illuminate\Http\Response
     */
    public function show(Employee $employee)
    {
        return view("employee.show", compact(
            "employee"
        ));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Employee  $employee
     * @return \Illuminate\Http\Response
     */
    public function edit(Employee $employee)
    {
        $position = Position::all();
        return view('employee.create', compact(
            "employee",
            "position"
        ));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Employee  $employee
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Employee $employee)
    {
        $validate = $this->validate($request, [
            "nama" => "string",
            "nip" => "unique:employees,nip,".$employee->id,
            "nomor_hp" => "numeric",
            "foto_ktp" => "mimes:png,jpg"
        ]);

        $data = $request->all();
        
        if ($request->foto_ktp) {
            $foto = Storage::disk('public_uploads')->put('img/file_upload', $request->foto_ktp);
            $data['foto_ktp'] = $foto;
        }
        $employee->update($data);
        return redirect()->back()->with("success", "Berhasil Merubah Data Karyawan");
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Employee  $employee
     * @return \Illuminate\Http\Response
     */
    public function destroy(Employee $employee)
    {
        $employee->delete();
        return response()->json(['success'=>'Berhasil Menghapus Karyawan']);
    }
}

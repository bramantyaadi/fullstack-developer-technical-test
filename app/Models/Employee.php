<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Employee extends Model
{
    use HasFactory;
    protected $fillable = [
        'position_id',
        'nama',
        'nip',
        "tgl_lahir",
        'tahun_lahir',
        'alamat',
        'nomor_hp',
        'agama',
        'foto_ktp',
        'status'
    ];

    /**
     * Get the user that owns the Employee
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function position()
    {
        return $this->belongsTo(Position::class, "position_id", "id");
    }   
}

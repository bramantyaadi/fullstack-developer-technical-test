@extends('layout.app')

@section('css')
    <link href="{{ asset('vendor/datatables/dataTables.bootstrap4.min.css') }}" rel="stylesheet">
@endsection
@section('breadcrumb')
    <div class="d-sm-flex align-items-center justify-content-between mb-4">
        <h1 class="h3 mb-0 text-gray-800">Karyawan</h1>
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="{{ url('/') }}">Dashboard</a></li>
            <li class="breadcrumb-item active" aria-current="page">Tambah Karyawan</li>
        </ol>
    </div>
@endsection
@section('content')
    <div class="card sm mb-4">
        <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
            <h6 class="m-0 font-weight-bold text-primary"></h6>
        </div>
        <div class="card-body">
            <div class="table-responsive p-3">
                <a href="{{ route('employee.create') }}" class="btn btn-primary mb-3" style="cursor: pointer">Tambah
                    Karyawan</a>

                <table class="table align-items-center table-flush" id="dataTable">
                    <thead class="thead-light">
                        <tr>
                            <th>No</th>
                            <th>Nama</th>
                            <th>Jabatan</th>
                            <th>NIP</th>
                            <th>Tahun Lahir</th>
                            <th>Alamat</th>
                            <th>Nomor Hp</th>
                            <th>Agama</th>
                            <th>Status</th>
                            <th>Aksi</th>
                        </tr>
                    </thead>
                </table>
            </div>
        </div>
    </div>
@endsection

@push('js')
    <script src="{{ asset('vendor/datatables/jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset('vendor/datatables/dataTables.bootstrap4.min.js') }}"></script>
    <script>
        var table = $('#dataTable').DataTable({
            processing: true,
            serverSide: true,
            ajax: "{{ route('employee.index') }}",
            columns: [{
                    data: 'DT_RowIndex',
                    orderable: false,
                    searchable: false,
                    className: "text-center"
                },
                {
                    data: 'nama'
                },
                {
                    data: 'position.name'
                },
                {
                    data: 'nip'
                },
                {
                    data: 'tahun_lahir'
                },
                {
                    data: 'alamat'
                },
                {
                    data: 'nomor_hp'
                },
                {
                    data: 'agama'
                },
                {
                    data: 'status'
                },
                {
                    data: 'action'
                }
            ]
        });

        $(document).ready(function() {

            $('body').on('click', '.deleteData', function() {

                var data_id = $(this).data("id");
                var token = $("meta[name='csrf-token']").attr("content");
                confirm("Are You sure want to delete !");

                $.ajax({
                    type: "DELETE",
                    url: "{{ url('employee') }}/" + data_id,
                    data: {
                        "id": data_id,
                        "_token": token,
                    },
                    success: function(data) {
                        table.draw();
                        alertSuccess(data.success)
                    },
                    error: function(data) {
                        console.log('Error:', data);
                    }
                });
            });
        });
    </script>
@endpush

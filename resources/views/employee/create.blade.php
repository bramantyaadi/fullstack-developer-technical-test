@extends('layout.app')

@section('css')
    <link href="{{ asset('vendor/datatables/dataTables.bootstrap4.min.css') }}" rel="stylesheet">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.2.0/css/datepicker.min.css" rel="stylesheet">
@endsection
@section('breadcrumb')
    <div class="d-sm-flex align-items-center justify-content-between mb-4">
        <h1 class="h3 mb-0 text-gray-800">Karyawan</h1>
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="{{ url('/') }}">Dashboard</a></li>
            <li class="breadcrumb-item active" aria-current="page">Tambah Karyawan</li>
        </ol>
    </div>
@endsection
@section('content')
    <div class="card sm mb-4">
        <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
            <h6 class="m-0 font-weight-bold text-primary"></h6>
        </div>
        <div class="card-body">
            <div class="table-responsive p-3">
                @if ($employee->id != null)
                <form action="{{ route('employee.update', $employee->id) }}" method="post" enctype="multipart/form-data">
                    @method("PUT")
                @else
                <form action="{{ route('employee.store') }}" method="post" enctype="multipart/form-data">
                @endif
                    @csrf
                    <div class="row">
                        <div class="col-md">
                            <div class="form-group">
                                <label for="exampleInputEmail1">Nama</label>
                                <input type="text" name="nama" value="{{ old("nama", $employee->nama) }}" class="form-control">
                            </div>
                        </div>
                        <div class="col-md">
                            <div class="form-group">
                                <label for="exampleInputEmail1">NIP</label>
                                <input type="number" name="nip" value="{{ old("nip", $employee->nip) }}" class="form-control">
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="exampleInputEmail1">Jabatan</label>
                        <select name="position_id" class="form-control">
                          <option value="">Pilih Jabatan</option>
                          @foreach ($position as $item)
                              <option value="{{ $item->id }}" {{ old("position_id", $employee->position_id) == $item->id ? "selected" : "" }}>{{ $item->name }}</option>
                          @endforeach
                        </select>
                    </div>
                    <div class="row">
                        <div class="col-md">
                            <div class="form-group">
                                <label for="exampleInputEmail1">Tanggal Lahir</label>
                                <input type="date" value="{{ old("tgl_lahir", $employee->tgl_lahir) }}" name="tgl_lahir" class="form-control">
                            </div>
                        </div>
                        <div class="col-md">
                            <div class="form-group">
                                <label for="exampleInputEmail1">Tahun Lahir</label>
                                <input class="date-own form-control" name="tahun_lahir" value="{{ old("tahun_lahir", $employee->tahun_lahir) }}" type="text">
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md">
                            <div class="form-group">
                                <label for="exampleInputEmail1">No. HP</label>
                                <input type="number" name="nomor_hp" value="{{ old("nomor_hp", $employee->nomor_hp) }}" class="form-control">
                            </div>
                        </div>
                        <div class="col-md">
                            <div class="form-group">
                                <label for="exampleInputEmail1">Alamat</label>
                                <textarea name="alamat" cols="3" rows="3" class="form-control">{{ old("alamat", $employee->alamat) }}</textarea>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                      <div class="col-md">
                        <label for="exampleInputEmail1">Foto KTP</label>
                        <input type="file" name="foto_ktp" class="form-control">
                      </div>
                      <div class="col-md">
                        <label for="exampleInputEmail1">Status</label>
                        <select name="status" class="form-control">
                          <option value="0" {{ old("status", $employee->status) == "0" ? "selected" : "" }}>Aktif</option>
                          <option value="1" {{ old("status", $employee->status) == "1" ? "selected" : "" }}>Tidak Aktif</option>
                        </select>
                      </div>
                      <div class="col-md-2">
                          <div class="form-group">
                              <label for="exampleInputEmail1">Agama</label>
                              <div class="form-check">
                                  <input class="form-check-input" type="radio" name="agama" id="exampleRadios1"
                                      value="islam" {{ old("agama", $employee->agama) == "islam" ? "checked" : "" }}>
                                  <label class="form-check-label" for="exampleRadios1">
                                      Islam
                                  </label>

                                  <input class="form-check-input ml-1" type="radio" name="agama" id="exampleRadios2"
                                      value="kristen" {{ old("agama", $employee->agama) == "kristen" ? "checked" : "" }}>
                                  <label class="form-check-label ml-4" for="exampleRadios2">
                                      Kristen
                                  </label>
                              </div>
                          </div>
                      </div>
                    </div>
                    <br>
                    <button type="submit" class="btn btn-primary">Submit</button>
                </form>
            </div>
        </div>
    </div>
@endsection

@push('js')
    <script src="{{ asset('vendor/datatables/jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset('vendor/datatables/dataTables.bootstrap4.min.js') }}"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
    <script src="https://netdna.bootstrapcdn.com/bootstrap/2.3.2/js/bootstrap.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.2.0/js/bootstrap-datepicker.min.js"></script>
    <script>
       $('.date-own').datepicker({
        format: "yyyy",
        viewMode: "years", 
        minViewMode: "years",
        autoclose:true //to close picker once year is selected
       });
    </script>
@endpush

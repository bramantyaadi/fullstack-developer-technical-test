@extends('layout.app')

@section('css')
    <link href="{{ asset('vendor/datatables/dataTables.bootstrap4.min.css') }}" rel="stylesheet">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.2.0/css/datepicker.min.css" rel="stylesheet">
@endsection
@section('breadcrumb')
    <div class="d-sm-flex align-items-center justify-content-between mb-4">
        <h1 class="h3 mb-0 text-gray-800">Detail Karyawan</h1>
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="{{ url('/') }}">Dashboard</a></li>
            <li class="breadcrumb-item active" aria-current="page">Detail Karyawan</li>
        </ol>
    </div>
@endsection
@section('content')
    <div class="row">
      <div class="col-md-4">
        <div class="card sm mb-4">
            <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
                <h6 class="m-0 font-weight-bold text-primary">Foto KTP</h6>
            </div>
            <div class="card-body">
                <div class="table-responsive p-3">
                  <img class="img-fluid" src="{{ asset("uploads/".$employee->foto_ktp) }}" alt="">
                </div>
            </div>
        </div>
      </div>
      <div class="col-md-8">
        <div class="card sm mb-4">
            <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
                <h6 class="m-0 font-weight-bold text-primary">Profile Karyawan</h6>
            </div>
            <div class="card-body">
                <div class="table-responsive p-3">
                  <div class="row">
                    <div class="col">
                      <div class="form-group row">
                        <label for="staticEmail" class="col-sm-2 col-form-label font-weight-bold">Nama</label>
                        <div class="col-sm-10">
                          <label class="form-control-plaintext">{{ $employee->nama }}</label>
                        </div>
                      </div>
    
                      <div class="form-group row">
                        <label for="staticEmail" class="col-sm-2 col-form-label font-weight-bold">NIP</label>
                        <div class="col-sm-10">
                          <label class="form-control-plaintext">{{ $employee->nip }}</label>
                        </div>
                      </div>
    
                      <div class="form-group row">
                        <label for="staticEmail" class="col-sm-2 col-form-label font-weight-bold">Jabatan</label>
                        <div class="col-sm-10">
                          <label class="form-control-plaintext">{{ $employee->position->name }}</label>
                        </div>
                      </div>

                      <div class="form-group row">
                        <label for="staticEmail" class="col-sm-2 col-form-label font-weight-bold">Tanggal Lahir</label>
                        <div class="col-sm-10">
                          <label class="form-control-plaintext">{{ $employee->tgl_lahir }}</label>
                        </div>
                      </div>
    
                      <div class="form-group row">
                        <label for="staticEmail" class="col-sm-2 col-form-label font-weight-bold">Tahun Lahir</label>
                        <div class="col-sm-10">
                          <label class="form-control-plaintext">{{ $employee->tahun_lahir }}</label>
                        </div>
                      </div>
                    </div>


                    <div class="col">
                      <div class="form-group row">
                        <label for="staticEmail" class="col-sm-2 col-form-label font-weight-bold">Agama</label>
                        <div class="col-sm-10">
                          <label class="form-control-plaintext">{{ $employee->agama }}</label>
                        </div>
                      </div>

                      <div class="form-group row">
                        <label for="staticEmail" class="col-sm-2 col-form-label font-weight-bold">Alamat</label>
                        <div class="col-sm-10">
                          <label class="form-control-plaintext">{{ $employee->alamat }}</label>
                        </div>
                      </div>

                      <div class="form-group row">
                        <label for="staticEmail" class="col-sm-2 col-form-label font-weight-bold">Status</label>
                        <div class="col-sm-10">
                          <label class="form-control-plaintext">{{ $employee->status == 0 ? "Aktif" : "Tidak Aktif" }}</label>
                        </div>
                      </div>
                    </div>
                  </div>
                  

                  
                </div>
            </div>
        </div>
      </div>
    </div>
@endsection

@push('js')

@endpush
